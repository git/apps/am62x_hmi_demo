import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3

Window {
    id: window
    visible: true
    visibility: "FullScreen"
    title: qsTr("AM62 HMI Demo")

    Rectangle {
        id: backgroundrect
        width: window.width
        height: window.height
        Image {
            id: backgroundimage
            source:"images/Background.png"
        }
        Image {
            id: motorimage
            source: "images/servo-drives-icon.png"
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.02
        }
    }

    Rectangle {
        id: tilogo
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.08
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.255

        Image {
            source: "images/Texas_Instrument.png"
        }
    }

    Rectangle {
        id: topright
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.15
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.35

        Image {
            source: "images/Top_Righ_Box.png"
        }
        Text {
            text: qsTr("Motor-1 RPM Control")
            color: "#FFFFFF"
            anchors.top: parent.top
            anchors.topMargin: window.height * 0.02
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.075
            font.pixelSize: 25
        }
        CircularGauge {
            id: motorspeed1
            value: 0
            maximumValue: 130
            anchors.top: parent.top
            anchors.topMargin: window.height * 0.07
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.03
            width: window.width * 0.22
            height: window.height * 0.22
            style: CircularGaugeStyle {
                needle: Rectangle {
                    implicitWidth: outerRadius * 0.02
                    implicitHeight: outerRadius * 0.60
                    antialiasing: true
                    color: "#D0001C"
                }
                foreground: Item {
                    Rectangle {
                        width: outerRadius * 0.1
                        height: width
                        radius: width / 2
                        color: "#FFFFFF"
                        anchors.centerIn: parent
                    }
                }
                tickmarkLabel:  Text {
                    visible: false
                }

                tickmark: Rectangle {
                    visible: false
                }

                minorTickmark: Rectangle {
                    visible: false
                }

                function degreesToRadians(degrees) {
                    return degrees * (Math.PI / 180);
                }
                background: Canvas {
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.reset();

                        ctx.beginPath();
                        ctx.strokeStyle = "#C6C6C6";
                        ctx.lineWidth = outerRadius * 0.04;

                        ctx.arc(outerRadius, outerRadius, outerRadius - ctx.lineWidth / 2,
                                degreesToRadians(valueToAngle(0) - 90), degreesToRadians(valueToAngle(130) - 90));
                        ctx.stroke();
                    }
                }
            }
        }
        Rectangle {
            id: increment1
            anchors.top: motorspeed1.bottom
            y: -10
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.102
            Image {
                id: rectbox1
                source: "images/Rectangle.png"
            }
            Text {
                id: motor1text
                x: 55
                y: 7
                text: qsTr("0")
                color: "#FFFFFF"
                font.pixelSize: 24
            }

            Rectangle {
                id: minus1
                anchors.left: increment1.left
                width: minusbox1.width
                height: minusbox1.height
                color: "transparent"
                Image {
                    id: minusbox1
                    source: "images/Minux_Box.png"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (powerbox.count == 1) {
                            motorspeed1.value -=10
                            motor1text.text = motorspeed1.value

                            if ( motorspeed1.value > 10 && motorspeed1.value < 130) {
                                motor1bar.width -= 52
                            }
                            else if (motorspeed1.value <= 10 && motorspeed1.value > 0) {
                                motor1bar.width = 10
                            }

                            else {
                                motor1bar.width = 0
                            }
                        }

                    }
                }
            }
            Rectangle {
                id: plus1
                x: 100
                width: plusbox1.width
                height: plusbox1.height
                color: "transparent"
                Image {
                    id: plusbox1
                    source: "images/Plus_Box.png"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (powerbox.count == 1) {
                            motorspeed1.value +=10
                            motor1text.text = motorspeed1.value
                            if ( motorspeed1.value == 10) {
                                motor1bar.width = 10
                            }
                            else if (motorspeed1.value >= 10 && motorspeed1.value < 130) {
                                motor1bar.width += 53
                            }

                            else if (motorspeed1.value == 130) {
                                motor1bar.width = 640
                            }

                            else {
                                motor1bar.width = 0
                            }
                        }
                    }
                }
            }

        }
    }

    Rectangle {
        id: bottomright
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.525
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.35

        Image {
            source: "images/Bottom_Right_Box.png"
        }
        Text {
            text: qsTr("Motor-2 RPM Control")
            color: "#FFFFFF"
            anchors.top: parent.top
            anchors.topMargin: window.height * 0.02
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.075
            font.pixelSize: 25
        }
        CircularGauge {
            id: motorspeed2
            value: 0
            maximumValue: 130
            anchors.top: parent.top
            anchors.topMargin: window.height * 0.07
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.03
            width: window.width * 0.22
            height: window.height * 0.22
            style: CircularGaugeStyle {
                needle: Rectangle {
                    implicitWidth: outerRadius * 0.02
                    implicitHeight: outerRadius * 0.60
                    antialiasing: true
                    color: "#D0001C"
                }
                foreground: Item {
                    Rectangle {
                        width: outerRadius * 0.1
                        height: width
                        radius: width / 2
                        color: "#FFFFFF"
                        anchors.centerIn: parent
                    }
                }
                tickmarkLabel:  Text {
                    visible: false
                }

                tickmark: Rectangle {
                    visible: false
                }

                minorTickmark: Rectangle {
                    visible: false
                }

                function degreesToRadians(degrees) {
                    return degrees * (Math.PI / 180);
                }
                background: Canvas {
                    onPaint: {
                        var ctx = getContext("2d");
                        ctx.reset();

                        ctx.beginPath();
                        ctx.strokeStyle = "#C6C6C6";
                        ctx.lineWidth = outerRadius * 0.04;

                        ctx.arc(outerRadius, outerRadius, outerRadius - ctx.lineWidth / 2,
                                degreesToRadians(valueToAngle(0) - 90), degreesToRadians(valueToAngle(130) - 90));
                        ctx.stroke();
                    }
                }
            }
        }
        Rectangle {
            id: increment2
            anchors.top: motorspeed2.bottom
            y: -10
            anchors.left: parent.left
            anchors.leftMargin: window.width * 0.102
            Image {
                id: rectbox2
                source: "images/Rectangle.png"
            }
            Text {
                id: motor2text
                x: 55
                y: 7
                text: qsTr("0")
                color: "#FFFFFF"
                font.pixelSize: 24
            }

            Rectangle {
                id: minus2
                anchors.left: increment2.left
                width: minusbox2.width
                height: minusbox2.height
                color: "transparent"
                Image {
                    id: minusbox2
                    source: "images/Minux_Box.png"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (powerbox.count == 1) {
                            motorspeed2.value -=10
                            motor2text.text = motorspeed2.value
                            if ( motorspeed2.value > 10 && motorspeed2.value < 130) {
                                motor2bar.width -= 52
                            }
                            else if (motorspeed2.value <= 10 && motorspeed2.value > 0) {
                                motor2bar.width = 10
                            }

                            else {
                                motor2bar.width = 0
                            }
                        }
                    }
                }
            }
            Rectangle {
                id: plus2
                x: 100
                width: plusbox2.width
                height: plusbox2.height
                color: "transparent"
                Image {
                    id: plusbox2
                    source: "images/Plus_Box.png"
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (powerbox.count == 1) {
                            motorspeed2.value +=10
                            motor2text.text = motorspeed2.value
                            if ( motorspeed2.value == 10) {
                                motor2bar.width = 10
                            }
                            else if (motorspeed2.value >= 10 && motorspeed2.value < 130) {
                                motor2bar.width += 53
                            }

                            else if (motorspeed2.value == 130) {
                                motor2bar.width = 640
                            }

                            else {
                                motor2bar.width = 0
                            }
                        }
                    }
                }
            }

        }
    }

    Rectangle {
        id: centerbox
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.15
        anchors.left: parent.left
        anchors.leftMargin: window.width * 0.14

        Image {
            source: "images/Center_Box.png"
        }

        Rectangle {
            id: tempcontrol
            Image {
                source: "images/Motor_Temp.png"
                anchors.top: parent.top
                anchors.topMargin: window.height * 0.05
                anchors.left: parent.left
                anchors.leftMargin: window.width * 0.10
            }

            Image {
                id: motor1image
                Text {
                    id: motor1
                    text: qsTr("Motor 1")
                    y: -100
                    color: "#FFFFFF"
                    font.pixelSize: 20
                }
                anchors.top: parent.top
                anchors.topMargin: window.height * 0.30
                anchors.left: parent.left
                anchors.leftMargin: window.width * 0.10
                source: "images/Repeat_Grid.png"

                Rectangle {
                    id: motor1tempbar
                    width: motor1image.width
                    height: motor1image.width * 0.05
                    color: "#A0A0A0"
                    anchors.top: motor1.bottom
                    anchors.topMargin: window.height * 0.02
                    Rectangle {
                        id: motor1bar
                        color: "#FFFFFF"
                        width: 0
                        height: parent.height
                    }
                }
            }

            Image {
                id: motor2image
                Text {
                    id: motor2
                    text: qsTr("Motor 2")
                    y: -100
                    color: "#FFFFFF"
                    font.pixelSize: 20
                }
                anchors.top: parent.top
                anchors.topMargin: window.height * 0.60
                anchors.left: parent.left
                anchors.leftMargin: window.width * 0.10
                source: "images/Repeat_Grid.png"

                Rectangle {
                    id: motor2tempbar
                    width: motor2image.width
                    height: motor2image.width * 0.05
                    color: "#A0A0A0"
                    anchors.top: motor2.bottom
                    anchors.topMargin: window.height * 0.02
                    Rectangle {
                        id: motor2bar
                        color: "#FFFFFF"
                        width: 0
                        height: parent.height
                    }
                }
            }
        }
    }

    Rectangle {
        id: controlpanel
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.18
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: window.height * 0.77
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.855
        color: "transparent"
        Image {
            id: controlimage
            source: "images/Control_Panel_with_text.png"
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.20
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.25
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                controlpanel.color = "#C6C6C6"
            }
            onExited: {
                controlpanel.color = "transparent"
            }
            onClicked: {
                textupdate.text = "Control Panel selected"
            }
        }
    }

    Rectangle {
        id: renderpanel
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.28
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: window.height * 0.67
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.855
        color: "transparent"
        Image {
            id: renderimage
            source: "images/3D_with_text.png"
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.20
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.25
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                renderpanel.color = "#C6C6C6"
            }
            onExited: {
                renderpanel.color = "transparent"
            }
            onClicked: {
                textupdate.text = "3D demo selected (Coming Soon)"
            }
        }
    }

    Rectangle {
        id: analyticspanel
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.38
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: window.height * 0.57
        anchors.right: parent.right
        anchors.rightMargin: window.width * 0.855
        color: "transparent"
        Image {
            id: analyticsimage
            source: "images/Analytics_with_text.png"
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.20
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.25
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onEntered: {
                analyticspanel.color = "#C6C6C6"
            }
            onExited: {
                analyticspanel.color = "transparent"
            }
            onClicked: {
                textupdate.text = "Analytics demo selected (Coming Soon)"
            }
        }
    }

    Rectangle {
        id: statusbox
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.05
        anchors.left: parent.left
        anchors.leftMargin: window.width * 0.145
        Image {
            id: statusheader
            source: "images/Status_Message.png"
        }
        Text {
            id: textupdate
            text: qsTr("Press the button to turn on.")
            anchors.top: statusbox.bottom
            anchors.topMargin: window.height * 0.05
            color: "#14AFC0"
            font.pixelSize: 36
        }


    }

    Rectangle {
        id: powerbox
        anchors.top: parent.top
        anchors.topMargin: window.height * 0.75
        anchors.left: parent.left
        anchors.leftMargin: window.width * 0.04
        width: powerimage.width
        height: powerimage.height
        color: "transparent"
        property int count: 0

        Image {
            id: powerimage
            source: "images/On_Button.png"
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(powerbox.count == 1) {
                    powerimage.source = "images/On_Button.png"
                    powerbox.count = 0
                    textupdate.text = "Off Button Pressed"
                    motorspeed1.value = 0
                    motor1text.text = motorspeed1.value
                    motor1bar.width = 0
                    motorspeed2.value = 0
                    motor2text.text = motorspeed2.value
                    motor2bar.width = 0


                }
                else {
                    powerimage.source = "images/Off_Button.png"
                    powerbox.count = 1
                    textupdate.text = "On Button Pressed"
                }

            }
        }

    }
}
